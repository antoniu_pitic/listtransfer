﻿namespace A01
{
    public class Course
    {
        string courseTitle;
        int grade;

        public string CourseTitle
        {
            get
            {
                return courseTitle;
            }

            set
            {
                courseTitle = value;
            }
        }

        public int Grade
        {
            get
            {
                return grade;
            }

            set
            {
                grade = value;
            }
        }
    }
}
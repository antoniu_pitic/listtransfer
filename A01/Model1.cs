namespace A01
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class Model1 : DbContext
    {
     public Model1()
            : base("name=Model1")
        {
        }

        //public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }

    }
}
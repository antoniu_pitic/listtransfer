﻿using System.Collections.Generic;

namespace A01
{
    public class Student
    {
        public int studentId;
        string name;
        string surname;
        int birthYear;
      //  List <Course> coursesList = new List<Course>();



        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }

            set
            {
                surname = value;
            }
        }

        public int BirthYear
        {
            get
            {
                return birthYear;
            }

            set
            {
                birthYear = value;
            }
        }

        //public List<Course> CoursesList
        //{
        //    get
        //    {
        //        return coursesList;
        //    }

        //    set
        //    {
        //        coursesList = value;
        //    }
        //}
    }
}
﻿namespace A01
{
    internal interface IDataProvider
    {
        void Load(string fileName);
        void Save(string fileName);
    }
}
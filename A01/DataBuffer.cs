﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A01
{
    class DataBuffer
    {
        List<string> list1 = new List<string>(GetWordsFromFile("ScufitaRosie.txt"));
        List<string> list2 = new List<string>(GetWordsFromFile("FatFrumos.txt"));

        private  static IEnumerable <string> GetWordsFromFile(string fileName) 
        {
            StreamReader reader = new StreamReader(fileName);
            return reader.ReadToEnd().Split(' ', '-');
        }

         public List<string> List1
        {
            get
            {
                return list1;
            }

        }

        public List<string> List2
        {
            get
            {
                return list2;
            }

        }
    }
}

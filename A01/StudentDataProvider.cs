﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A01
{
     class StudentDataProvider:IDataProvider
    {
        static List<Student> studentList = new List<Student> ();

        public List<Student> GetAllStudents()
        {
            return studentList;
        }

        internal static List<Student> StudentList
        {
            get
            {
                return studentList;
            }

            set
            {
                studentList = value;
            }
        }

        public void Load(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            string serializedList = reader.ReadToEnd();

            studentList =  
                JsonConvert.DeserializeObject<List <Student>>(serializedList);

            reader.Close();
        }

        public void Save(string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);

            string serializedList  
                = JsonConvert.SerializeObject(studentList, Formatting.Indented);

            writer.Write(serializedList);

            writer.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace A01
{
    public partial class Form1 : Form
    {
        StudentDataProvider db = new StudentDataProvider();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db.Load("listaStudenti.txt");
            listBox1.DataSource = db.GetAllStudents();
            listBox1.DisplayMember = "Name";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataBuffer db = new DataBuffer();
            Misc.L1 = db.List1.Where(x => x.StartsWith("z")).ToList();
            Misc.L2 = db.List2.Where(x => x.StartsWith("h")).ToList();
            RefreshForm();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Misc.MoveFromL1toL2(
                listBox1.SelectedItems.Cast<string>().ToList());

            RefreshForm();

        }

        private void RefreshForm()
        {
            listBox1.DataSource = null;
            listBox1.DataSource = Misc.L1;
        }

        private void button3_Click(object sender, EventArgs e)
        {

            RefreshForm();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            db.Save("listaStudenti.txt");
        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateStudentInfo();
        }

        private void PopulateStudentInfo()
        {
            nameTextBox.Text = ((Student)listBox1.SelectedItem).Name;
            surnameTextBox.Text = ((Student)listBox1.SelectedItem).Surname;
            birthYearTextBox.Text = ((Student)listBox1.SelectedItem).BirthYear.ToString();

            //coursesListDataGridView.DataSource
            //    = ((Student)listBox1.SelectedItem).CoursesList;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            using (var db = new Model1())
            {
                db.Courses.Add(new Course
                {
                    CourseTitle = "C++",
                    Grade = 10
                });
                db.SaveChanges();
            }
        }
    }
}

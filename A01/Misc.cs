﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace A01
{
    static class Misc
    {
        static List<string> l1 = new List<string>();
        static List<string> l2 = new List<string>();

        static public List<string> L1
        {
            get
            {
                return l1;
            }

            set
            {
                l1 = value;
            }
        }

        static public List<string> L2
        {
            get
            {
                return l2;
            }

            set
            {
                l2 = value;
            }
        }
        internal static void MoveFrom(List<string> L1, List<string> L2, List<string> selectedItems)
        {
            L2.AddRange(selectedItems);
            L1.RemoveAll(x => selectedItems.Contains(x));
        }
        internal static void MoveFromL1toL2(List<string> selectedItems)
        {
            MoveFrom(L1, L2, selectedItems);
        }
        internal static void MoveFromL2toL1(List<string> selectedItems)
        {
            MoveFrom(L2, L1, selectedItems);
        }

    }
}
